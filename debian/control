Source: xhtml2pdf
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Martin <debacle@debian.org>
Build-Depends: debhelper-compat (= 13),
    xsltproc,
    docbook-xsl
Build-Depends-Indep: dh-python,
    python3-all,
    python3-setuptools,
    python3-pytest,
    python3-cherrypy3,
    python3-html5lib,
    python3-pil,
    python3-pypdf,
    python3-reportlab (>= 4.1.0),
    python3-bidi,
    python3-arabic-reshaper,
    pybuild-plugin-pyproject,
    python3-asn1crypto,
    python3-pyhanko-certvalidator,
    python3-svglib
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://github.com/xhtml2pdf/xhtml2pdf
Vcs-Git: https://salsa.debian.org/python-team/packages/xhtml2pdf.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/xhtml2pdf

Package: python3-xhtml2pdf
Architecture: all
Provides: ${python3:Provides}
Depends: ${python3:Depends}, ${misc:Depends}
Description: Converts HTML into PDFs using ReportLab
 xhtml2pdf is a html2pdf converter using the ReportLab Toolkit, the
 HTML5lib and pyPdf. It supports HTML 5 and CSS 2.1 (and some of CSS
 3). It is completely written in pure Python so it is platform
 independent.
 .
 The main benefit of this tool that a user with Web skills like HTML
 and CSS is able to generate PDF templates very quickly without
 learning new technologies.
 .
 xhtml2pdf was previously developed as "pisa".
 .
 This package installs the library for Python 3.
