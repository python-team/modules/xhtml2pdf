Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: xhtml2pdf
Source: https://github.com/xhtml2pdf/xhtml2pdf
Files-Excluded: manual_test/font/*
Comment: The fonts have various additional license terms
         and are not built from source.

Files: *
Copyright: 2010 Dirk Holtwick <dirk.holtwick@gmail.com>
           2012-2015 Chris Glass
           2015-2016 Benjamin Bach
           2016-Current Sam Spencer
License: Apache-2.0

License: Apache-2.0
 See /usr/share/common-licenses/Apache-2.0.

Files: xhtml2pdf/reportlab_paragraph.py
Copyright: ReportLab Europe Ltd. 2000-2008
License: BSD-3-clause
 Deduced from http://www.reportlab.com/ftp/ReportLab_2_4.tar.gz
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 
 * Neither the name of the company nor the names of its contributors may be
   used to endorse or promote products derived from this software without
   specific prior written permission. 
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE OFFICERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Files: xhtml2pdf/w3c/css.py       xhtml2pdf/w3c/__init__.py
       xhtml2pdf/w3c/cssParser.py xhtml2pdf/w3c/cssDOMElementInterface.py
Copyright: 2002-2004 TechGame Networks, LLC.
           All rights reserved.
Comment:
 The CSS parser functionality available in this submodule has originally been
 developed by TechGame Networks, LLC as part of their TG Framework module.
 .
 All changes made to these files are subject to the general xhtml2pdf license,
 id est Apache-2.0 (see the `LICENSE.txt` file in the repository root for
 details).
License: BSD-3-clause~TechGame and Apache-2.0

License: BSD-3-clause~TechGame
 Redistribution and use in source and binary forms, with or without modification, are permitted
 provided that the following conditions are met:
 .
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
 .
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation and/or
      other materials provided with the distribution.
 .
    * Neither the name of TechGame Networks, LLC nor the names of its contributors may
      be used to endorse or promote products derived from this software without specific
      prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
